# merge-our-misc cron.d file
MAILTO="root@localhost"
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
LOG="/var/log/merge-our-misc.log"

# min        hour         day mon wday user     cmd
15           0           *   *   *    mom      (cd /var/lib/merge-our-misc/ && /usr/share/merge-our-misc/main.py) >> "$LOG" 2>&1
